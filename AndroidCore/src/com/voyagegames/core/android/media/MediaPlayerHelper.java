package com.voyagegames.core.android.media;

import java.io.IOException;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;

public class MediaPlayerHelper {
	
	public static void release(final MediaPlayer player) {
		if (player == null) {
			return;
		}
		
		if (player.isPlaying()) {
			player.stop();
		}
		
		player.release();
	}
	
	protected final Context mContext;
	
	protected MediaPlayer mPlayer;
	
	public MediaPlayerHelper(final Context context) {
		mContext = context;
		mPlayer = new MediaPlayer();
	}
	
	public MediaPlayerHelper(final Context context, final MediaPlayer player) {
		mContext = context;
		mPlayer = player;
	}
	
	public MediaPlayer player() {
		return mPlayer;
	}
	
	public void release() {
		release(mPlayer);
	}

	public void play(final int resourceID) {
		release(mPlayer);
		mPlayer = MediaPlayer.create(mContext, resourceID);
		mPlayer.start();
	}

	public void play(final Uri uri) {
		release(mPlayer);
		mPlayer = MediaPlayer.create(mContext, uri);
		mPlayer.start();
	}

	public void play(final int resourceID, final int millisecondSeek) {
		release(mPlayer);
		mPlayer = MediaPlayer.create(mContext, resourceID);
		mPlayer.seekTo(millisecondSeek);
		mPlayer.start();
	}

	public void play(final Uri uri, final int millisecondSeek) {
		release(mPlayer);
		mPlayer = MediaPlayer.create(mContext, uri);
		mPlayer.seekTo(millisecondSeek);
		mPlayer.start();
	}
	
	public void prepareLocalUri(final Uri uri, final int streamType)
			throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
		release(mPlayer);
		mPlayer.setAudioStreamType(streamType);
		mPlayer.setDataSource(mContext, uri);
		mPlayer.prepare();
	}
	
	public void prepareAsyncLocalUri(final Uri uri, final int streamType)
			throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
		release(mPlayer);
		mPlayer.setAudioStreamType(streamType);
		mPlayer.setDataSource(mContext, uri);
		mPlayer.prepare();
	}
	
	public void prepareRemoteURL(final String url, final int streamType)
			throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
		release(mPlayer);
		mPlayer.setAudioStreamType(streamType);
		mPlayer.setDataSource(url);
		mPlayer.prepareAsync();
	}
	
	public void prepareAsyncRemoteURL(final String url, final int streamType)
			throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
		release(mPlayer);
		mPlayer.setAudioStreamType(streamType);
		mPlayer.setDataSource(url);
		mPlayer.prepareAsync();
	}

}
