package com.voyagegames.core.android.audio;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.voyagegames.core.android.modules.AbstractCursorConsumer;

public class SongPicker extends AbstractCursorConsumer<Song> {
	
	public List<Song> getByArtistID(final Context context, final String artistID) {
		final String selection = MediaStore.Audio.Media.ARTIST_ID + "=" + artistID + " AND " + MediaStore.Audio.Media.IS_MUSIC + "!=0";
		final String[] projection = {
				MediaStore.Audio.Media.DATA,
				MediaStore.Audio.Media._ID,
				MediaStore.Audio.Media.ARTIST_ID,
				MediaStore.Audio.Media.ARTIST,
				MediaStore.Audio.Media.TITLE
			};
		
		return super.getList(context, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, selection);
	}
	
	public List<Song> getBySongID(final Context context, final String songID) {
		final String selection = MediaStore.Audio.Media._ID + "=" + songID;
		final String[] projection = {
				MediaStore.Audio.Media.DATA,
				MediaStore.Audio.Media._ID,
				MediaStore.Audio.Media.ARTIST_ID,
				MediaStore.Audio.Media.ARTIST,
				MediaStore.Audio.Media.TITLE
			};
		
		return super.getList(context, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, selection);
	}

	@Override
	public Song consume(final Cursor cursor) {
		final String uri = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
		final long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
		final long artistID = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID));
		final String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
		final String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
		
		if (artist == null || title == null) {
			return null;
		}
		
		return new Song(uri, id, artistID, artist, title);
	}

}
