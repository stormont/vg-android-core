package com.voyagegames.core.android.audio;

public class Artist {
	
	public final long id;
	public final String name;
	
	public Artist(final long id, final String name) {
		this.id = id;
		this.name = name;
	}
}
