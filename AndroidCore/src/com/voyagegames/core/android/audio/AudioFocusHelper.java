package com.voyagegames.core.android.audio;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

public class AudioFocusHelper implements AudioManager.OnAudioFocusChangeListener {
	
	protected final Context mContext;
	
	protected AudioManager mManager;
	protected MediaPlayer mPlayer;
	protected float mNormalLeftVolume;
	protected float mNormalRightVolume;
	protected float mSubduedLeftVolume;
	protected float mSubduedRightVolume;
	
	public AudioFocusHelper(final Context context, final MediaPlayer player) {
		mContext = context;
		
		mManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		mPlayer = player;
		mNormalLeftVolume = 1.0f;
		mNormalRightVolume = 1.0f;
		mSubduedLeftVolume = 0.1f;
		mSubduedRightVolume = 0.1f;
	}
	
	public void setNormalVolume(final float leftVolume, final float rightVolume) {
		mNormalLeftVolume = leftVolume;
		mNormalRightVolume = rightVolume;
	}
	
	public void setSubduedVolume(final float leftVolume, final float rightVolume) {
		mSubduedLeftVolume = leftVolume;
		mSubduedRightVolume = rightVolume;
	}
	
	public boolean requestFocus() {
		return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == mManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
	}
	
	public boolean abandonFocus() {
		return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == mManager.abandonAudioFocus(this);
	}

	@Override
	public void onAudioFocusChange(final int focusChange) {
	    switch (focusChange) {
	        case AudioManager.AUDIOFOCUS_GAIN:
	        	// Resume playback
	            if (!mPlayer.isPlaying()) {
	            	mPlayer.start();
	            }
	            
	            mPlayer.setVolume(mNormalLeftVolume, mNormalRightVolume);
	            break;
	
	        case AudioManager.AUDIOFOCUS_LOSS:
	            // Lost focus for an unbounded amount of time: stop playback and release media player
	            if (mPlayer.isPlaying()) {
	            	mPlayer.stop();
	            }
	            
	            mPlayer.release();
	            mPlayer = null;
	            break;
	
	        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
	            // Lost focus for a short time, but we have to stop playback. We don't release the media player because playback is likely to resume
	            if (mPlayer.isPlaying()) {
	            	mPlayer.pause();
	            }
	            
	            break;
	
	        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
	            // Lost focus for a short time, but it's ok to keep playing
	            // at an attenuated level
	            if (mPlayer.isPlaying()) {
	            	mPlayer.setVolume(mSubduedLeftVolume, mSubduedRightVolume);
	            }
	            
	            break;
	    }
	}

}
