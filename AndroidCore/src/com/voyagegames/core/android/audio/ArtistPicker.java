package com.voyagegames.core.android.audio;

import java.util.List;

import com.voyagegames.core.android.modules.AbstractCursorConsumer;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

public class ArtistPicker extends AbstractCursorConsumer<Artist> {
	
	public List<Artist> getAll(final Context context) {
		final String[] projection = {
				MediaStore.Audio.Artists._ID,
				MediaStore.Audio.Artists.ARTIST
			};
		
		return super.getList(context, MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, projection, null);
	}
	
	public List<Artist> getByArtistID(final Context context, final String artistID) {
		final String selection = MediaStore.Audio.Artists._ID + "=" + artistID;
		final String[] projection = {
				MediaStore.Audio.Artists._ID,
				MediaStore.Audio.Artists.ARTIST
			};
		
		return super.getList(context, MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, projection, selection);
	}

	@Override
	public Artist consume(final Cursor cursor) {
		final long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Artists._ID));
		final String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST));
		
		if (name == null) {
			return null;
		}
		
		return new Artist(id, name);
	}

}
