package com.voyagegames.core.android.audio;

public class Song {
	
	public final String uri;
	public final long id;
	public final long artistID;
	public final String artist;
	public final String title;
	
	public Song(final String uri, final long id, final long artistID, final String artist, final String title) {
		this.uri = uri;
		this.id = id;
		this.artistID = artistID;
		this.artist = artist;
		this.title = title;
	}

}
