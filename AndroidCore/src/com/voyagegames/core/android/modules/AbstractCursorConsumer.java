package com.voyagegames.core.android.modules;

import java.util.ArrayList;
import java.util.List;

import com.voyagegames.core.android.interfaces.ICursorConsumer;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public abstract class AbstractCursorConsumer <T> implements ICursorConsumer<T> {
	
	public List<T> getList(
			final Context context,
			final Uri uri,
			final String[] projection,
			final String selection) {
		final Cursor cursor = context.getContentResolver().query(
				uri,
				projection,
				selection,
				null,
				null);
		
		final List<T> results = new ArrayList<T>();
		
		if (cursor == null) {
			return results;
		}
		
		if (!cursor.moveToFirst()) {
			cursor.close();
			return results;
		}
		
		do {
			try {
				final T result = consume(cursor);
				
				if (result == null) {
					continue;
				}
				
				results.add(result);
			} catch (final Exception e) {
				Log.e(AbstractCursorConsumer.class.getName(), "Bad consumer supplied");
				cursor.close();
				return results;
			}
		} while (cursor.moveToNext());
		
		cursor.close();
		return results;
	}

}
