package com.voyagegames.core.android.modules;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

public class AppManager {
	
	public static Intent findAvailableIntent(final Context context, final String action) {
	    final PackageManager packageManager = context.getPackageManager();
	    final Intent intent = new Intent(action);
	    
	    if (packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0) {
	    	return intent;
	    }
	    
	    return null;
	}
	
	public static void dispatchIntent(final Activity activity, final Intent intent, final int actionCode) {
	    activity.startActivityForResult(intent, actionCode);
	}

}
