package com.voyagegames.core.android.interfaces;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public interface ICursorConsumer <T> {

	List<T> getList(Context context, Uri uri, String[] projection, String selection);
	T consume(Cursor cursor);
	
}
