package com.voyagegames.core.android.camera;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import com.voyagegames.core.android.modules.AppManager;

public class PhotoManager {
	
	private static final String JPEG_FILE_PREFIX = ".jpg";
	
	public File getPhotoDirectory(final String albumName) {
		return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), albumName);
	}
	
	public boolean dispatchCameraIntent(final Activity activity, final int actionCode, final File resultFile) {
		final Intent intent = AppManager.findAvailableIntent(activity, MediaStore.ACTION_IMAGE_CAPTURE);
		
		if (intent == null) {
			return false;
		}
		
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(resultFile));
		AppManager.dispatchIntent(activity, intent, actionCode);
		return true;
	}
	
	public Bitmap handleThumbnailPhoto(final Intent intent) {
	    final Bundle extras = intent.getExtras();
	    
	    if (extras == null) {
	    	return null;
	    }
	    
	    return (Bitmap)extras.get("data");
	}
	
	public File createImageFile(final File resultFile) throws IOException {
	    final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
	    final String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
	    
	    return File.createTempFile(imageFileName, JPEG_FILE_PREFIX, resultFile);
	}
	
	public void addImageToGallery(final Context context, final File file) {
	    final Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
	    final Uri contentUri = Uri.fromFile(file);
	    
	    intent.setData(contentUri);
	    context.sendBroadcast(intent);
	}
	
	public Bitmap generateScaledImage(final String imagePath, final int targetWidth, final int targetHeight) {
	    final BitmapFactory.Options bmOptions = new BitmapFactory.Options();
	    
	    bmOptions.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(imagePath, bmOptions);
	    
	    final int photoW = bmOptions.outWidth;
	    final int photoH = bmOptions.outHeight;
	  
	    final int scaleFactor = Math.min(photoW / targetWidth, photoH / targetHeight);
	  
	    bmOptions.inJustDecodeBounds = false;
	    bmOptions.inSampleSize = scaleFactor;
	    bmOptions.inPurgeable = true;
	  
	    return BitmapFactory.decodeFile(imagePath, bmOptions);
	}

}
